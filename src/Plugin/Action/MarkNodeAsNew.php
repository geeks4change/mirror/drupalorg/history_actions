<?php

namespace Drupal\history_actions\Plugin\Action;

use Drupal\Core\Action\ActionBase;
use Drupal\node\NodeInterface;

/**
 * Action description.
 *
 * @Action(
 *   id = "history_actions_new",
 *   label = @Translation("Mark as new."),
 *   type = "node"
 * )
 */
class MarkNodeAsNew extends ActionBase {

  use MarkNodeTrait;

  /**
   * {@inheritDoc}
   */
  public function execute($node = NULL) {
    if ($node instanceof NodeInterface) {
      $this->historyWriteTimestamp($node->id(), 0);
      $this->invalidateListCacheTag($node);
    }
  }

}
